<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Genre;

class Game extends Model
{
    protected $guarded = [];

    public function scopeFilter($query, $filters) {
        if ($user = $filters['user'] ?? false) {
            $query->where('user_id', '=', $user);
        }

        if ($genre = $filters['genre'] ?? false) {
            $query->whereHas('genres', function($q) use ($genre) {
                $q->where('genres.id', '=', $genre);
            });
        }
    }

    public function genres() {
    	return $this->belongsToMany('App\Genre');
	}

	public function comments() {
		return $this->hasMany('App\Comment');
	}

	public function user() {
    	return $this->belongsTo('App\User');
    }

	public function addComment($content) {
		$this->comments()->create([
			'content' => $content
		]);
	}
}
