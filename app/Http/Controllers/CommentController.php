<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Game;
use App\Comment;

class CommentController extends Controller
{
    public function store(Request $request, Game $game) {
    	$validated = $request->validate([
            'content' => 'required|min:5'
        ]);

    	$game->addComment($request->content);

    	return back();
    }
}
