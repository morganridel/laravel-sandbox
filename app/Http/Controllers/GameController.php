<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Genre;

class GameController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index() {
        $games = Game::with('genres')->filter(request(['user', 'genre']))->get()->toArray();

        return view('games.index', compact('games'));
    }

    public function show(Game $game) {
        return view('games.show', compact('game'));
    }

    public function add() {
        $genres = Genre::pluck('name', 'id');

        return view('games.add', compact('genres'));
    }

    public function store(Request $request) {


        $validated = $request->validate([
            'title' => 'required',
            'score' => ['required','numeric','regex:(^(?:[0-9](?:\.5)?|10)$)'],
            'notes' => '',
            'hours' => 'nullable|integer|min:0',
            'genre' => 'required|numeric|exists:genres,id'
        ]);

        $genre = Genre::find($request->genre);

        $game = Game::create([
            'name' => $request->title,
            'score' => $request->score,
            'notes' => $request->notes,
            'hours' => $request->hours ?? 0,
            'user_id' => auth()->id()
        ]);

        $game->genres()->attach($genre->id);

        return redirect('/');
    }
}
