<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Laravel Sandbox</title>
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>

        @yield('content')

    </body>
</html>
