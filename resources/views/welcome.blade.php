@extends('layout.master')

@section('content')
<h1>Hello World !</h1>

@if (Auth::check())
<h2>Logged as {{ Auth::user()->name }}</h2>
@endif

<p><a href="/register">Register</a></p>
<p><a href="/login">Login</a></p>

<p><a href="games">See your games</a></p>
<p><a href="games/add">Add a new game</a></p>

@endsection
