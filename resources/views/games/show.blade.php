@extends('layout.master')

@section('content')

<h1>{{ $game->name }}</h1>
<p>Created by : {{ $game->user->name }}</p>
<h2>Game notes</h2>
<p>{{ $game->notes }}</p>

<hr>

<div class="postComment">
	@if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
	
	<form method="post" action="/games/{{ $game->id }}/comments">
		{{ csrf_field() }}

		<textarea name="content" placeholder="Write your comment !" class="form-control"></textarea>

		<button type=submit class="btn btn-primary">Publish Comment</button>

	</form>

</div>

<hr>

<div class="comments">
	
	@foreach($game->comments as $comment)
	<div class="comment">
		<p><strong>{{ $comment->created_at->diffForHumans() }} :</strong></p>
		{{ $comment->content }}
	</div>
	@endforeach

</div>

@endsection
