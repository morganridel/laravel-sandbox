@extends('layout.master')

@section('content')

<p>dsqdqsd</p>
<table>
	<tr>
		<th>Title</th>
		<th>Genre</th>
		<th>Score</th>
		<th>Hours played</th>
		<th>Notes</th>
	</tr>
	@foreach($games as $key => $game)
	<tr>
		<td><a href="/games/{{ $game['id'] }}">{{ $game['name'] }}</a></td>
		<td>
		@foreach($game['genres'] as $key => $genre)
			{{ $genre['name'] }} 
		@endforeach
		</td>
		<td>{{ $game['score'] }}</td>
		<td>{{ $game['hours'] }}</td>
		<td>{{ $game['notes'] }}</td>
	</tr>
	@endforeach
</table>
@endsection
