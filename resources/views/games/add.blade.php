@extends('layout.master')

@section('content')
<h1>Add a new game</h1>

<form action="/games" method="post">
    {{ csrf_field() }}

    <label for="title">Title : </label>
    <input type="text" name="title">
    <br/>

    <label for="genre">Genre : </label>
    <select name="genre">
    @foreach($genres as $id => $genre)
      <option value="{{$id}}">{{$genre}}</option>
    @endforeach
    </select>
    <br/>
    
    <label for="score">Score : </label>
    <input name="score" type="range" step="0.5" min="0" max="10">
    <br/>

    <label for="hours">Hours played : </label>
    <input type="text" name="hours">
    <br/>

    <label for="notes">Notes : </label>
    <textarea name="notes"></textarea>
    <br/>
    
    <input type="submit">
    @if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</form>
@endsection
