@extends('layout.master')

@section('content')
<h1>Register</h1>


<form method='post' action='/register'>

	<div class="container">
		{{ csrf_field() }}

		<div class="form-group">
			<label for='name'>Name : </label>
			<input type="text" class=form-control name="name">
		</div>

		<div class="form-group">
			<label for='email'>Email : </label>
			<input type="email" class=form-control name="email">
		</div>

		<div class="form-group">
			<label for='password'>password : </label>
			<input type="password" class=form-control name="password">
		</div>

		<div class="form-group">
			<label for='password_confirmation'>Password confirmation : </label>
			<input type="password" class=form-control name="password_confirmation">
		</div>

		<div class="form-group">
			<button type='submit' class="btn btn-primary">Register</button>
		</div>

		@if ($errors->any())
		<div>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

	</div>
</form>

@endsection